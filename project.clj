(defproject compojure-template "0.1.0"
  :description "Simple Compojure template"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.8"]
                 [hiccup "1.0.5"]]
  :plugins [[lein-ring "0.8.10"]]
  :ring {:handler compojure-template.routes/app})
