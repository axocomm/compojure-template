(ns compojure-template.views
  (:use [hiccup core page]))

(def css
  ["bootstrap.min.css" "styles.css"])

(def js
  ["jquery-2.1.0.min.js" "bootstrap.min.js"])

(def nav-items
  [{:id "home" :href "/" :text "Home"}
   {:id "about" :href "#" :text "About"}])

(defn render [& content]
  content)

(defn layout-nav []
  "Display the navigation bar."
  [:div {:class "navbar navbar-inverse navbar-fixed-top" :id "navbar"}
   [:div {:class "container"}
    [:div {:class "navbar-header"}
     [:button {:type "button" :class "navbar-toggle"
               :data-toggle "collapse" :data-target ".navbar-collapse"}
      [:span {:class "icon-bar"}]
      [:span {:class "icon-bar"}]
      [:span {:class "icon-bar"}]]
     [:a {:class "navbar-brand" :href "#"} "Foo"]]
    [:div {:class "navbar-collapse collapse" :role "navigation"}
     [:ul {:class "nav navbar-nav"}
      (for [nav-item nav-items]
        [:li {:id (:id nav-item)}
         [:a {:href (:href nav-item)} (:text nav-item)]])]]]])

(defn layout [& {:keys [title extra-css extra-js content]
                 :or {title "" content "" extra-css [] extra-js []}}]
  (html5
   [:head
    [:meta {:charset "utf-8"}]
    [:title title]
    (map #'include-css (map #(str "css/" %)
                            (into css extra-css)))
    (map #'include-js (map #(str "js/" %)
                           (into js extra-js)))]
   [:body
    (layout-nav)
    [:div {:class "container" :id "main"}
     content]
    [:footer "Copyright &copy; 2014 " [:a {:href "#"} "Foo"]]]))

(defn index-page []
  (layout :title "Home"
          :content (render
                    [:h2 "Hello"])))
